<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tasklist;
use App\Models\category;

// q3
class TasklistController extends Controller
{
	public function read(){
    // response with tasklist datasets
    return tasklist::join('category','tasklist.categoryid','=','category.id')->get(['tasklist.*','category.description as categorydescription'])->toJson();
	}

	public function create(Request $req){
		try{
        	$tsk = new tasklist;
      		$tsk->description = $req->description;
      		$tsk->categoryid = $req->categoryid;
      		$tsk->save();
      	}catch (\Exception $e){
        	 if($e->errorInfo[1] == 2627){
            	return json_encode(['response'=>'error']);
         	}
      	}
      	return json_encode(['response'=>'ok']);
		
	}

	public function edit(Request $req){
		$tsk = tasklist::find($req->id);
    $tsk->description = $req->description;
    $tsk->categoryid = $req->categoryid;
    $tsk->save();
    return json_encode(['response'=>'ok']);
	}

	public function delete(Request $req){
		$tsk = tasklist::find($req->id);
      	$tsk->delete();
      	return json_encode(['response'=>'ok']);
	}
    
}
