<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// q2 simple class
class IntroController extends Controller
{
    public function hello($argval = "DL Ideas"){
    	return view('q2',['data'=>"Hello ".$argval.". Nice to meet you"]);
    }
}
