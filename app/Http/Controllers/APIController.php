<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

// q1
class APIController extends Controller
{
    public function read(){
    	$usersURL = "https://dlideas.local/users.json";
    	$todosURL = "https://dlideas.local/todos.json";

    	$client = new Client();
    	$response = $client->request('GET', $usersURL, ['verify'  => false,]);
    	$users = json_decode($response->getBody());
    	$response = $client->request('GET', $todosURL, ['verify'  => false,]);
    	$todos = json_decode($response->getBody());
    	// merge two api datasets
    	foreach ($todos as $key => $todo) {
    		foreach ($users as $key => $user) {
    			if($user->id == $todo->userId){
    				$todo->name = $user->name;
					$todo->username = $user->username;
					$todo->email = $user->email;
					$todo->phone = $user->phone;
					$todo->website = $user->website;
					$todo->address = array(
						'street' => $user->address->street,
						'suite' => $user->address->suite,
						'city' => $user->address->city,
						'zipcode' => $user->address->zipcode,
						'geo' => array(
							'lat' => $user->address->geo->lat,
							'lng' => $user->address->geo->lng
						),
						
					);
					$todo->company = array(
						'name' => $user->company->name, 
						'catchPhrase' => $user->company->catchPhrase, 
						'bs' => $user->company->bs, 
					);
	
    			}
    		}
    	}

    	return response()->json($todos);
    }
}



    