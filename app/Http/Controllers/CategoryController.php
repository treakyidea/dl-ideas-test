<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;

class CategoryController extends Controller
{
	// response with category datasets
	public function read(){
		return category::all()->toJson();
	}
}
