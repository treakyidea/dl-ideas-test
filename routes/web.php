<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('q1');});
Route::get('/q1', function () {return view('q1');});
Route::get('/q1api','APIController@read');

Route::get('/q2','IntroController@hello');

Route::get('/q3', function () {return view('q3');});
Route::get('/category','CategoryController@read');
Route::get('/tasklist','TasklistController@read');
Route::post('/tasklist-create','TasklistController@create');
Route::post('/tasklist-edit','TasklistController@edit');
Route::post('/tasklist-delete','TasklistController@delete');

