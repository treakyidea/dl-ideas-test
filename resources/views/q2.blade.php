@extends('layouts.app')
@section('content')
<div class="row" style="margin-top: 10px">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">Introduction</div>
			<div class="card-body">
				<!-- data pass from IntroController -->
       			{{$data}}
      		</div>
    	</div>
  	</div>
</div>
@endsection