@extends('layouts.app')
@section('content')
<div class="row" style="margin-top: 10px">
	<div class="col-md-4">
		<div class="card">
			<div class="card-header">Top Completed Todo Users</div>
			<div class="card-body">
		      <div class="table-responsive" style="font-size: 14px">
		        <table class="table table-success" class="display" id="tutable" style="width: 100%">
		          <thead>
		            <tr>
		              <th>User ID</th>
		              <th>Name</th>
		              <th>Completed task</th>
		            </tr>
		          </thead>
		        </table>
		      </div>
			</div>
		 </div>
	</div>
	<div class="col-md-8">
		<div class="card">
			<div class="card-header">Search Todo</div>
			<div class="card-body">
		      <div class="table-responsive" style="font-size: 12px">
		        <table class="table table-success" class="display" id="dttable" style="width: 100%">
		          <thead>
		            <tr>
		              <th>User ID</th>
		              <th>Name</th>
		              <th>Username</th>
		              <th>Email</th>
		              <th>Address</th>
		              <th>Coordinate</th>
		              <th>Phone</th>
		              <th>Website</th>
		              <th>Company</th>
		              <th>Title</th>
		              <th>Completed</th>
		            </tr>
		          </thead>
		        </table>
		      </div>
			</div>
		 </div>
	</div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function(){
	var usrdata,tdodata
	// calling api directly, the json file stored locally to avoid api hosted having issues. It happen during developement of the project
	$.ajax({
        'async': false,
        'type': "GET",
        'url': "https://dlideas.local/users.json",
        // 'url': "https://jsonplaceholder.typicode.com/users",
        'success': function (data) {usrdata = data}
    });
    $.ajax({
        'async': false,
        'type': "GET",
        'url': "https://dlideas.local/todos.json",
        'success': function (data) {tdodata = data;}
    });

    // using filter method to search for users and count the completed task
    for (var i = usrdata.length - 1; i >= 0; i--) {
    	usrdata[i].completedcount = tdodata.filter(tdodata => tdodata.userId ==	usrdata[i].id && tdodata.completed == true ).length
    }

    // task user table that display 10 users and order by top completed task
	var tutable = $("#tutable").DataTable({
      data: usrdata,
      columns: [
			{ "data": "id" },
			{ "data": "name" },
			{ "data": "completedcount" },
      ],
      "scrollY": "400px",
      "scrollCollapse": true,	
		 "order": [[ 2, "desc" ]]
    })
    
    // endpoint api is in APIController which then being used as datatabales searching function
	var dttable = $("#dttable").DataTable({
      	"ajax": {
      		"url":"https://dlideas.local/q1api",
      		"dataSrc":""
      	},
      	"columns": [
			{ "data": "userId" },
			{ "data": "name" },
			{ "data": "username" },
			{ "data": "email" },
			{ 
				"data": "address"
				,render: function ( data, type, row ) {
					return data.suite +' '+ data.street+'<br>'+data.zipcode+' '+data.city;
				}
			},
			{ 
				"data": "address"
				,render: function ( data, type, row ) {
					return data.geo.lat +' '+ data.geo.lng;
				}
			},
			{ "data": "phone" },
			{ "data": "website" },
			{
				"data":"company"
				,render: function ( data, type, row ) {
					return data.name +'<br><small><b>'+ data.catchPhrase+'</b></small>';
				}
			},
			{ "data": "title" },
			{ "data": "completed" },
      	],
    	"columnDefs": [
    		{ "width": "15%", "targets": 4 },
    		{ "width": "15%", "targets": 8 },
    		{ "width": "10%", "targets": 9 }
  		],
  		"scrollY": "400px",
  		"scrollCollapse": true,	
    })
});
</script>
@endsection