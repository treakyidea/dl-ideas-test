@extends('layouts.app')
@section('content')
<div class="row" style="margin-top: 10px">
	<div class="col-md-12">
		<div class="card">
      <div class="card-header">Task Management</div>
			<div class="card-body">
		      <div class="table-responsive" style="font-size: 14px">
		        <table class="table table-success" class="display" id="tasktable" style="width: 100%">
		          <thead>
		            <tr>
		              <th>task ID</th>
		              <th>Description</th>
		              <th>Category</th>
		            </tr>
		          </thead>
		        </table>
		      </div>
      		</div>
    	</div>
  	</div>
</div>
<!-- modal for create and edit task -->
<div class="modal" tabindex="-1" role="dialog" id="newmodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<form>
      		<input type="hidden" name="action" id="actioninput" value="">
      		<input type="hidden" name="id" id="idinput" value="">
		  	<div class="form-group">
		    	<label for="descriptioninput">Description</label>
		    	<textarea class="form-control" id="descriptioninput" placeholder="what is your task?"></textarea>
		  	</div>
		  	<div class="form-group">
		    	<label for="categoryselect">category</label>
		        <select class="form-control" id="categoryselect"><option disabled="disabled" selected="selected">choose</option></select>
		  	</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="newmodalsavebtn">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- modal for delete task, asking for confirmation -->
<div class="modal" tabindex="-1" role="dialog" id="deletemodal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<p>Are you sure?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="deletemodalBtn">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript" src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var categorydata
  // store category data in client
	$.ajax({
        'async': false,
        'type': "GET",
        'url': "{{ url('category') }}",
        'success': function (data) {
        	var html
        	categorydata = JSON.parse(data)
        	for (var i = categorydata.length - 1; i >= 0; i--) {
        		html += '<option value='+categorydata[i].id+'>'+categorydata[i].description+'</option>'
        	}
        	$('#categoryselect').append(html)
        }
    });
    // when user cancelling the modal the selection will be reset
  	$('#modalno').click(function(){
    	tasktable.rows({selected:true}).deselect()
  	})
    // handling create and edit function
  	$('#newmodalsavebtn').click(function(){
  		if($('#actioninput').val() == 'edit'){
  			var datapost = { id:$('#idinput').val(),description: $('#descriptioninput').val(), categoryid: $('#categoryselect').val()}
  			$.post( "{{ url('tasklist-edit') }}", datapost, function() {
	     	 	$('#newmodal').modal('toggle')
	      		tasktable.ajax.reload()
	    	})
  		}else{
  			var datapost = { description: $('#descriptioninput').val(), categoryid: $('#categoryselect').val()}
	  		$.post( "{{ url('tasklist-create') }}", datapost, function() {
	     	 	$('#newmodal').modal('toggle')
	      		tasktable.ajax.reload()
	    	})
  		}
  	})
    // handling delete function
  	$('#deletemodalBtn').click(function(){
  			$.post( "{{ url('tasklist-delete') }}", {id:tasktable.rows({selected:true}).data()[0].id}, function() {
	     	 	$('#deletemodal').modal('toggle')
	      		tasktable.ajax.reload()
	    	})
  	})

  	var editmodal = function(){
  		$('#idinput').val(tasktable.rows({selected:true}).data()[0].id)	
  		$('#descriptioninput').val(tasktable.rows({selected:true}).data()[0].description)	
  		$('#categoryselect').val(tasktable.rows({selected:true}).data()[0].categoryid)
  		$('#actioninput').val('edit')
  		$('#newmodal').modal('toggle')
  	}

	var tasktable = $("#tasktable").DataTable({
	    dom: 'Bfrtip',
	    select: {
	            style: 'single'
	    },
	    buttons: [
	    	{text: 'New', className: 'newBtn', action: function ( e, dt, node, config ) { $('#newmodal').modal('toggle') }, enabled:true },
	    	{text: 'Edit', className: 'editBtn', action: function ( e, dt, node, config ) { editmodal() } , enabled:false },
	    	{text: 'Delete', className: 'deleteBtn', action: function ( e, dt, node, config ) { $('#deletemodal').modal('toggle') } , enabled:false },
	    ],
	    ajax: {
	      'url': "{{ url('tasklist') }}",
	      'dataSrc':''
	    },
	    rowId:"id",
	    "columns": [
	          { "data": "id" },
	          { "data": "description" },
	          { "data": "categorydescription" },
	    ],
	    "order": [[ 0, "desc" ]]
	  	}).on('select.dt', function() {
	    	$('.newBtn').addClass('disabled')
	    	$('.editBtn').removeClass('disabled')
	    	$('.deleteBtn').removeClass('disabled')
	  	}).on('deselect.dt', function() {
	    	$('.newBtn').removeClass('disabled')
	    	$('.editBtn').addClass('disabled')
	    	$('.deleteBtn').addClass('disabled')
	  	})

	});
</script>
@endsection